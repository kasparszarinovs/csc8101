-- Artists
INSERT INTO artist VALUES (nextval('artist_id_seq'), 'Imagine Dragons'), (nextval('artist_id_seq'), 'fun.'), (nextval('artist_id_seq'), 'Shinedown'), (nextval('artist_id_seq'), 'Datsik'), (nextval('artist_id_seq'), 'Black Stone Cherry'), (nextval('artist_id_seq'), 'The xx'), (nextval('artist_id_seq'), 'Downlink'), (nextval('artist_id_seq'), 'Royksopp');

-- Albums
INSERT INTO album VALUES (nextval('album_id_seq'), 'd48c2bd4e8e1b5e88c69d95a6063b5a0', 'Amaryllis', 2012, 3);
INSERT INTO album VALUES (nextval('album_id_seq'), '33187f824ba6a014a6dd01d93c8289c8', 'Night visions', 2012, 1);
INSERT INTO album VALUES (nextval('album_id_seq'), '7aa986d6cde4922ce77de55ab97b09d6', 'Continued Silence EP', 2011, 1);

-- Tracks
INSERT INTO track VALUES (nextval('track_id_seq'), '/downloads/740-Shinedown-Adrenaline.mp3', '740e71342ce2682a3a277ea56006d961', 'Adrenaline', 2012, 3);
INSERT INTO track VALUES (nextval('track_id_seq'), '/downloads/a7d-Shinedown-Bully.mp3', 'a7dfdbb3decf5d0b5737817fe03d116a', 'Bully', 2012, 3);
INSERT INTO track VALUES (nextval('track_id_seq'), '/downloads/d48-Shinedown-Amaryllis.mp3', 'd48c2bd4e8e1b5e88c69d95a6063b5a0', 'Amaryllis', 2012, 3);
INSERT INTO track VALUES (nextval('track_id_seq'), '/downloads/9e2-Shinedown-Unity.mp3', '9e22528278f3d52829d6614429c5709c', 'Unity', 2012, 3);
INSERT INTO track VALUES (nextval('track_id_seq'), '/downloads/677-Shinedown-Enemies.mp3', '677d0dc368856102d67e906ce44ceb1e', 'Enemies', 2012, 3);
INSERT INTO track VALUES (nextval('track_id_seq'), '/downloads/42b-Shinedown-I''m not alright.mp3', '42bf6790b1e7e24078152d13af86a043', 'I''m not alright', 2012, 3);
INSERT INTO track VALUES (nextval('track_id_seq'), '/downloads/8ba-Shinedown-Nowhere kids.mp3', '8ba9e9dfec5b50a6faa31cd19dcff8aa', 'Nowhere kids', 2012, 3);
INSERT INTO track VALUES (nextval('track_id_seq'), '/downloads/ffd-Shinedown-Miracle.mp3', 'ffdb2cfc15262d746c374cd2ae0228ea', 'Miracle', 2012, 3);
INSERT INTO track VALUES (nextval('track_id_seq'), '/downloads/fd7-Shinedown-I''ll follow you.mp3', 'fd706589fffa80394947c8910975a6c3', 'I''ll follow you', 2012, 3);
INSERT INTO track VALUES (nextval('track_id_seq'), '/downloads/e73-Shinedown-For my sake.mp3', 'e736cf39168ae6af81b8008a55f9f851', 'For my sake', 2012, 3);
INSERT INTO track VALUES (nextval('track_id_seq'), '/downloads/0b7-Shinedown-My name.mp3', '0b7cdd2fcdde2cb84d3e64d8976e2ae0', 'My name', 2012, 3);
INSERT INTO track VALUES (nextval('track_id_seq'), '/downloads/bb7-Shinedown-Through the ghost.mp3', 'bb77df13a7254deabd402c3f033a3c63', 'Through the ghost', 2012, 3);

INSERT INTO track VALUES (nextval('track_id_seq'), '/downloads/ad6-Imagine Dragons-Radioactive.mp3', 'ad6c1eb1076a42adf255959be5f66bdc', 'Radioactive', 2012, 1);
INSERT INTO track VALUES (nextval('track_id_seq'), '/downloads/900-Imagine Dragons-Tiptoe.mp3', '900de57898c197b8037cf6c6f2a31c61', 'Tiptoe', 2012, 1);
INSERT INTO track VALUES (nextval('track_id_seq'), '/downloads/945-Imagine Dragons-It''s Time.mp3', '945a4d713efc5bbaa0f5ee5e4e38d68b', 'It''s Time', 2012, 1);
INSERT INTO track VALUES (nextval('track_id_seq'), '/downloads/886-Imagine Dragons-Demons.mp3', '886d5fec17581543ea13a8c59d439d1c', 'Demons', 2012, 1);
INSERT INTO track VALUES (nextval('track_id_seq'), '/downloads/7c2-Imagine Dragons-On Top of the World.mp3', '7c23eb950fee191a50a87f1757b93004', 'On Top of the World', 2012, 1);
INSERT INTO track VALUES (nextval('track_id_seq'), '/downloads/752-Imagine Dragons-Amsterdam.mp3', '752632a732f4f0fabefadb2599823712', 'Amsterdam', 2012, 1);
INSERT INTO track VALUES (nextval('track_id_seq'), '/downloads/330-Imagine Dragons-Hear Me.mp3', '33029ecdf0cafc0862ff4cce841fe2a0', 'Hear Me', 2012, 1);
INSERT INTO track VALUES (nextval('track_id_seq'), '/downloads/039-Imagine Dragons-Every Night.mp3', '0396bb985113293dda9181ca88fadd2b', 'Every Night', 2012, 1);
INSERT INTO track VALUES (nextval('track_id_seq'), '/downloads/ef0-Imagine Dragons-Bleeding Out.mp3', 'ef0412114756fe5e52759dc4355b53b4', 'Bleeding Out', 2012, 1);
INSERT INTO track VALUES (nextval('track_id_seq'), '/downloads/983-Imagine Dragons-Underdog.mp3', '9837313d8ae18ee2388e0185033e22cd', 'Underdog', 2012, 1);
INSERT INTO track VALUES (nextval('track_id_seq'), '/downloads/104-Imagine Dragons-Nothing Left to Say.mp3', '104d316c274279aa825b5a43162d36e5', 'Nothing Left to Say', 2012, 1);
INSERT INTO track VALUES (nextval('track_id_seq'), '/downloads/9d2-Imagine Dragons-Working Man.mp3', '9d2423713aa7111d1d2ed40b61833a9d', 'Working Man', 2012, 1);
INSERT INTO track VALUES (nextval('track_id_seq'), '/downloads/d05-Imagine Dragons-Fallen.mp3', 'd05f6559eca83b22d6f9c39deaec01f0', 'Fallen', 2012, 1);

INSERT INTO track VALUES (nextval('track_id_seq'), '/downloads/68d-Imagine Dragons-Round and Round.mp3', '68dc1c3a0d3d6a800f027bcc48365f24', 'Round and Round', 2011, 1);
INSERT INTO track VALUES (nextval('track_id_seq'), '/downloads/bae-Imagine Dragons-My Fault.mp3', 'baeffa17203bf91ce575cb619d6764a2', 'My Fault', 2011, 1);

-- Tracks x Albums
INSERT INTO track_album VALUES (1,1), (2,1), (3,1), (4,1), (5,1), (6,1), (7,1), (8,1), (9,1), (10,1), (11,1), (12,1);
INSERT INTO track_album VALUES (13,2), (14,2), (15,2), (16,2), (17,2), (18,2), (19,2), (20,2), (21,2), (22,2), (23,2), (24,2), (25,2);
INSERT INTO track_album VALUES (26,3), (27,3);

-- Downloads
INSERT INTO download VALUES (nextval('download_id_seq'), 'now', 2);
INSERT INTO download VALUES (nextval('download_id_seq'), 'now', 8);
INSERT INTO download VALUES (nextval('download_id_seq'), 'now', 2);
INSERT INTO download VALUES (nextval('download_id_seq'), 'now', 5);
INSERT INTO download VALUES (nextval('download_id_seq'), 'now', 2);
INSERT INTO download VALUES (nextval('download_id_seq'), 'now', 4);
INSERT INTO download VALUES (nextval('download_id_seq'), 'now', 2);
INSERT INTO download VALUES (nextval('download_id_seq'), 'now', 3);
INSERT INTO download VALUES (nextval('download_id_seq'), 'now', 2);
INSERT INTO download VALUES (nextval('download_id_seq'), 'now', 1);
INSERT INTO download VALUES (nextval('download_id_seq'), 'now', 2);
INSERT INTO download VALUES (nextval('download_id_seq'), 'now', 1);
INSERT INTO download VALUES (nextval('download_id_seq'), 'now', 2);
INSERT INTO download VALUES (nextval('download_id_seq'), 'now', 8);
INSERT INTO download VALUES (nextval('download_id_seq'), 'now', 2);
INSERT INTO download VALUES (nextval('download_id_seq'), 'now', 5);
INSERT INTO download VALUES (nextval('download_id_seq'), 'now', 2);
INSERT INTO download VALUES (nextval('download_id_seq'), 'now', 4);
INSERT INTO download VALUES (nextval('download_id_seq'), 'now', 2);
INSERT INTO download VALUES (nextval('download_id_seq'), 'now', 3);
INSERT INTO download VALUES (nextval('download_id_seq'), 'now', 2);
INSERT INTO download VALUES (nextval('download_id_seq'), 'now', 1);
INSERT INTO download VALUES (nextval('download_id_seq'), 'now', 2);
INSERT INTO download VALUES (nextval('download_id_seq'), 'yesterday', 1);
INSERT INTO download VALUES (nextval('download_id_seq'), 'yesterday', 2);
INSERT INTO download VALUES (nextval('download_id_seq'), 'yesterday', 7);
INSERT INTO download VALUES (nextval('download_id_seq'), 'yesterday', 16);
INSERT INTO download VALUES (nextval('download_id_seq'), 'yesterday', 23);
INSERT INTO download VALUES (nextval('download_id_seq'), 'yesterday', 11);
INSERT INTO download VALUES (nextval('download_id_seq'), 'yesterday', 3);
INSERT INTO download VALUES (nextval('download_id_seq'), 'yesterday', 1);
INSERT INTO download VALUES (nextval('download_id_seq'), 'yesterday', 1);
INSERT INTO download VALUES (nextval('download_id_seq'), 'yesterday', 2);
INSERT INTO download VALUES (nextval('download_id_seq'), 'yesterday', 7);
INSERT INTO download VALUES (nextval('download_id_seq'), 'yesterday', 16);
INSERT INTO download VALUES (nextval('download_id_seq'), 'yesterday', 23);
INSERT INTO download VALUES (nextval('download_id_seq'), 'yesterday', 11);
INSERT INTO download VALUES (nextval('download_id_seq'), 'yesterday', 3);
INSERT INTO download VALUES (nextval('download_id_seq'), 'yesterday', 1);
INSERT INTO download VALUES (nextval('download_id_seq'), 'yesterday', 1);
INSERT INTO download VALUES (nextval('download_id_seq'), 'yesterday', 2);
INSERT INTO download VALUES (nextval('download_id_seq'), 'yesterday', 7);
INSERT INTO download VALUES (nextval('download_id_seq'), 'yesterday', 16);
INSERT INTO download VALUES (nextval('download_id_seq'), 'yesterday', 23);
INSERT INTO download VALUES (nextval('download_id_seq'), 'yesterday', 11);
INSERT INTO download VALUES (nextval('download_id_seq'), 'yesterday', 3);
INSERT INTO download VALUES (nextval('download_id_seq'), 'yesterday', 1);
INSERT INTO download VALUES (nextval('download_id_seq'), 'yesterday', 1);
INSERT INTO download VALUES (nextval('download_id_seq'), 'yesterday', 2);
INSERT INTO download VALUES (nextval('download_id_seq'), 'yesterday', 7);
INSERT INTO download VALUES (nextval('download_id_seq'), 'yesterday', 16);
INSERT INTO download VALUES (nextval('download_id_seq'), 'yesterday', 23);
INSERT INTO download VALUES (nextval('download_id_seq'), 'yesterday', 11);
INSERT INTO download VALUES (nextval('download_id_seq'), 'yesterday', 3);
INSERT INTO download VALUES (nextval('download_id_seq'), 'yesterday', 1);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 1);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 2);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 7);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 16);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 23);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 11);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 3);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 1);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 1);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 2);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 7);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 16);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 23);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 11);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 3);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 1);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 1);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 2);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 7);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 16);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 23);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 11);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 3);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 1);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 1);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 2);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 7);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 16);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 23);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 11);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 3);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 1);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 1);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 2);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 7);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 16);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 23);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 11);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 3);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 1);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 1);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 2);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 7);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 16);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 23);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 11);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 3);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 1);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 1);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 2);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 7);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 16);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 23);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 11);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 3);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 1);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 1);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 2);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 7);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 16);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 23);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 11);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 3);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 1);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 1);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 2);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 7);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 16);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 23);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 11);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 3);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 1);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 1);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 2);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 7);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 16);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 23);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 11);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 3);
INSERT INTO download VALUES (nextval('download_id_seq'), '2012-12-04', 1);










