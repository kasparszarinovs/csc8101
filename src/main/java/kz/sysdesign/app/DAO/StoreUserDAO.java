package kz.sysdesign.app.DAO;

import kz.sysdesign.app.Entities.StoreUser;

/**
 * Store user DAO interface.
 * 
 * @author Kaspars Zarinovs &lt;k.zarinovs@ncl.ac.uk&gt;
 *
 */
public interface StoreUserDAO {
	
	/**
	 * Adds new user to database.
	 * 
	 * @param user the user object
	 * @return <code>true</code> if successful, <code>false</code> if unsuccessful
	 */
	public boolean addUser(StoreUser user);
	
	/**
	 * Gets user's salt from database by username
	 * 
	 * @param loginName username
	 * @return salt or <code>null</code> if user does not exist in the database
	 */
	public String getSalt(String loginName);
	
	/**
	 * Authenticates the user
	 * 
	 * @param loginName username
	 * @param loginPassword password
	 * @return user credentials or <code>null</code> if user does not exist in the database
	 */
	public StoreUser authenticate(String loginName, String loginPassword);
}
