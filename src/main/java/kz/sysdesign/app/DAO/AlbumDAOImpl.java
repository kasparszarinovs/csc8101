package kz.sysdesign.app.DAO;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import kz.sysdesign.app.Entities.Album;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of Album DAO.
 * Methods support database interaction for album data.
 * 
 * @author Kaspars Zarinovs &lt;k.zarinovs@ncl.ac.uk&gt;
 *
 */
@Repository
public class AlbumDAOImpl implements AlbumDAO {

	@PersistenceContext(unitName="JpaPersistenceUnit")
	private EntityManager em;

	/**
	 * @see kz.sysdesign.app.DAO.AlbumDAO#addAlbum(kz.sysdesign.app.Entities.Album)
	 */
	@Transactional
	public boolean addAlbum(Album album)
	{
		if(album == null)
			return false;

		em.persist(album);
		return true;
	}

	/**
	 * @see kz.sysdesign.app.DAO.AlbumDAO#getAlbumByName(java.lang.String)
	 */
	public Album getAlbumByName(String name)
	{
		String jpqlQuery = "FROM " + Album.class.getSimpleName() + " a WHERE a.name = :name";
		Query query = em.createQuery(jpqlQuery, Album.class).setParameter("name", name);

		Album album = null;
		try
		{
			album = (Album) query.getSingleResult();
		}
		catch(NonUniqueResultException ue) {  }
		catch(NoResultException e) {  }

		return album;
	}

	/**
	 * @see kz.sysdesign.app.DAO.AlbumDAO#getAlbumNamesByPattern(java.lang.String)
	 */
	public List<String> getAlbumNamesByPattern(String pattern)
	{
		String jpqlQuery = "SELECT a.name FROM " + Album.class.getSimpleName() + " a WHERE lower(a.name) LIKE lower(:pattern)";
		Query query = em.createQuery(jpqlQuery).setParameter("pattern", "%" + pattern + "%");

		// If no results exist, empty list is returned
		List<String> resultList = (List<String>) query.getResultList();

		return resultList;
	}

	/**
	 * @see kz.sysdesign.app.DAO.AlbumDAO#getAllAlbums()
	 */
	public List<Album> getAllAlbums()
	{
		String jpqlQuery = "FROM " + Album.class.getSimpleName();
		Query query = em.createQuery(jpqlQuery);

		// If no results exist, empty list is returned
		List<Album> resultList = (List<Album>) query.getResultList();

		return resultList;
	}

}
