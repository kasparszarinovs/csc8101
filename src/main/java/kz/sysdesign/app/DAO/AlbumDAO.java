package kz.sysdesign.app.DAO;

import java.util.List;

import kz.sysdesign.app.Entities.Album;

/**
 * Album DAO interface.
 * 
 * @author Kaspars Zarinovs &lt;k.zarinovs@ncl.ac.uk&gt;
 *
 */
public interface AlbumDAO {

	/**
	 * Adds album to database.
	 * Transactional.
	 * 
	 * @param album album object
	 * @return <code>true</code> if album has been successfully added to the database,
	 * <code>false</code> if unsuccessful
	 */
	public boolean addAlbum(Album album);
	
	/**
	 * Gets album from database by name.
	 * 
	 * @param name name of the album
	 * @return album object
	 */
	public Album getAlbumByName(String name);
	
	/**
	 * Gets album names from database which satisfies pattern matching.
	 * 
	 * @param pattern
	 * @return list of album names
	 */
	public List<String> getAlbumNamesByPattern(String pattern);
	
	/**
	 * Gets all albums from database and returns their objects.
	 * 
	 * @return list of all albums
	 */
	public List<Album> getAllAlbums();
	
}
