package kz.sysdesign.app.DAO;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import kz.sysdesign.app.Entities.Artist;

/**
 * Implementation of Artist DAO
 * Methods support database interaction for artist data.
 * 
 * @author Kaspars Zarinovs &lt;k.zarinovs@ncl.ac.uk&gt;
 *
 */
@Repository
public class ArtistDAOImpl implements ArtistDAO {

	@PersistenceContext(unitName="JpaPersistenceUnit")
	private EntityManager em;

	/**
	 * @see kz.sysdesign.app.DAO.ArtistDAO#addArtist(kz.sysdesign.app.Entities.Artist)
	 */
	@Transactional
	public boolean addArtist(Artist artist)
	{
		if(artist == null)
			return false;

		em.persist(artist);
		return true;
	}

	/**
	 * @see kz.sysdesign.app.DAO.ArtistDAO#getArtistByName(java.lang.String)
	 */
	public Artist getArtistByName(String name)
	{
		String jpqlQuery = "FROM " + Artist.class.getSimpleName() + " a WHERE LOWER(a.name) = LOWER(:name)";
		Query query = em.createQuery(jpqlQuery, Artist.class).setParameter("name", name);

		Artist artist = null;
		try
		{
			artist = (Artist) query.getSingleResult();
		}
		catch(NonUniqueResultException ue) {  }
		catch(NoResultException e) {  }

		return artist;
	}

	/**
	 * @see kz.sysdesign.app.DAO.ArtistDAO#getArtistNamesByPattern(java.lang.String)
	 */
	public List<String> getArtistNamesByPattern(String pattern)
	{
		String jpqlQuery = "SELECT a.name FROM " + Artist.class.getSimpleName() + " a WHERE lower(a.name) LIKE lower(:pattern)";
		Query query = em.createQuery(jpqlQuery).setParameter("pattern", "%" + pattern + "%");

		// If no results are found, returns empty list
		List<String> resultList = (List<String>) query.getResultList();

		return resultList;
	}

	/**
	 * @see kz.sysdesign.app.DAO.ArtistDAO#getAllArtists()
	 */
	public List<Artist> getAllArtists()
	{
		String jpqlQuery = "FROM " + Artist.class.getSimpleName();
		Query query = em.createQuery(jpqlQuery);

		// If no results are found, returns empty list
		List<Artist> resultList = (List<Artist>) query.getResultList();

		return resultList;
	}

}