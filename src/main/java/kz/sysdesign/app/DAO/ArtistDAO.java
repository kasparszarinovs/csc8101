package kz.sysdesign.app.DAO;

import java.util.List;

import kz.sysdesign.app.Entities.Artist;

/**
 * Artist DAO interface
 * 
 * @author Kaspars Zarinovs &lt;k.zarinovs@ncl.ac.uk&gt;
 *
 */
public interface ArtistDAO {
	
	/**
	 * Adds new artist to the database.
	 * Transactional.
	 * 
	 * @param artist artist object
	 * @return <code>true</code> if successfull, <code>false</code> if unsuccessfull
	 */
	public boolean addArtist(Artist artist);
	
	/**
	 * Gets artist data by name.
	 * 
	 * @param name name of the artist
	 * @return artist object
	 */
	public Artist getArtistByName(String name);
	
	/**
	 * Gets artists that satisfy the pattern.
	 * 
	 * @param pattern partial of full name of the artist
	 * @return list of artists that match the pattern
	 */
	public List<String> getArtistNamesByPattern(String pattern);
	
	/**
	 * Gets all artists.
	 * 
	 * @return list of all artists
	 */
	public List<Artist> getAllArtists();
	
}
