package kz.sysdesign.app.DAO;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import kz.sysdesign.app.Entities.StoreUser;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation for Store user DAO
 * Methods support database interaction for store user data.
 * 
 * @author Kaspars Zarinovs &lt;k.zarinovs@ncl.ac.uk&gt;
 *
 */
@Repository
public class StoreUserDAOImpl implements StoreUserDAO {

	@PersistenceContext(unitName="JpaPersistenceUnit")
	private EntityManager em;

	/**
	 * @see kz.sysdesign.app.DAO.StoreUserDAO#addUser(kz.sysdesign.app.Entities.StoreUser)
	 */
	@Transactional
	public boolean addUser(StoreUser storeUser)
	{		
		if(storeUser == null)
			return false;

		em.persist(storeUser);
		return true;
	}

	/**
	 * @see kz.sysdesign.app.DAO.StoreUserDAO#authenticate(java.lang.String, java.lang.String)
	 */
	public StoreUser authenticate(String loginName, String loginPassword) {
		if(loginName == null || loginPassword == null)
			return null;

		final String queryString = "FROM " + StoreUser.class.getSimpleName() + " u WHERE WHERE LOWER(u.email) = LOWER(:loginName) AND u.password = :loginPassword";
		Query query = em.createQuery(queryString).setParameter("loginName", loginName).setParameter("loginPassword", loginPassword);

		StoreUser user = null;
		try
		{
			user = (StoreUser) query.getSingleResult();
		}
		catch(NonUniqueResultException nure) {  }
		catch(NoResultException nre) {  }

		return user;
	}

	/**
	 * @see kz.sysdesign.app.DAO.StoreUserDAO#getSalt(java.lang.String)
	 */
	public String getSalt(String loginName)
	{
		final String queryString = "SELECT u.salt FROM " + StoreUser.class.getSimpleName() + " u WHERE LOWER(u.email) = LOWER(:loginName)";
		Query query = em.createQuery(queryString).setParameter("loginName", loginName);

		String salt = null;
		try 
		{
			salt = (String) query.getSingleResult();
		}
		catch(NonUniqueResultException nure) {  }
		catch(NoResultException nre) {  }

		return salt;
	}

}

