package kz.sysdesign.app.DAO;

import java.util.List;

import kz.sysdesign.app.Entities.Download;
import kz.sysdesign.app.Entities.Track;

/**
 * Track DAO interface.
 * 
 * @author Kaspars Zarinovs &lt;k.zarinovs@ncl.ac.uk&gt;
 *
 */
public interface TrackDAO {
	
	/**
	 * Add track to database.
	 * 
	 * @param track track object
	 * @return <code>true</code> if successful, <code>false</code> if unsuccessful
	 */
	public boolean addTrack(Track track);
	
	/**
	 * Gets tracks that partially or completely matches pattern.
	 * 
	 * @param pattern name pattern
	 * @return list of tracks
	 */
	public List<Object[]> getTracksByPattern(String pattern);
	
	/**
	 * Gets all tracks.
	 * 
	 * @return list of all tracks
	 */
	public List<Object[]> getAllTracks();

    /**
     * Gets top 10 tracks.
     * 
     * @return top 10 tracks
     */
    public List<Object[]> getTopTenTracks();

    /**
     * Gets track by it's MD5 checksum.
     * 
     * @param checksum
     * @return track
     */
    public Track getTrackByChecksum(String checksum);

    /**
     * Increases download counter for a track.
     * 
     * @param download download object
     * @return <code>true</code> if successful, <code>false</code> if unsuccessful
     */
    public boolean addDownload(Download download);

    /**
     * Gets all tracks.
     * 
     * @return list of all tracks
     */
    public List<Track> getAllTracksList();
    
}
