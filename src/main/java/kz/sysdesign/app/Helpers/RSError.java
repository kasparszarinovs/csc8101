package kz.sysdesign.app.Helpers;

/**
 * Error helper class.
 * 
 * @author Kaspars Zarinovs &lt;k.zarinovs@ncl.ac.uk&gt;
 *
 */
public final class RSError {

	private static final String UNDEFINED_MSG = "Oh dear. An unknown error has occured!";

	private static final String EMPTY_INPUT_ERROR = "emptyInputError";
	private static final String EMPTY_INPUT_ERROR_MSG = "All fields are required.";
	private static final String INVALID_INPUT_ERROR = "invalidInputError";
	private static final String INVALID_INPUT_ERROR_MSG = "Invalid input paramteter(-s).";

	private static String message;

	/**
	 * Returns an error message based on the error type.
	 * 
	 * @param type error type
	 * @return error message
	 */
	public static String getErrorMessage(String type) 
	{
		if(type.equals(EMPTY_INPUT_ERROR))
			message = EMPTY_INPUT_ERROR_MSG;
		else if(type.equals(INVALID_INPUT_ERROR))
			message = INVALID_INPUT_ERROR_MSG;
		else
			message = UNDEFINED_MSG;

		return message;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() 
	{
		return message;
	}

}
