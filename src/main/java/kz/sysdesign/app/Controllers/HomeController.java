package kz.sysdesign.app.Controllers;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import kz.sysdesign.app.Entities.Download;
import kz.sysdesign.app.Entities.Track;
import kz.sysdesign.app.Services.TrackService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

/**
 * Handles requests for the application home page, search page,
 * browse page, top page & download page.
 * 
 * @author Kaspars Zarinovs &lt;k.zarinovs@ncl.ac.uk&gt;
 */
@Controller
public class HomeController {

	/**
	 * Autowires track service
	 */
	@Autowired
	private TrackService trackService;

	/**
	 * Generates home page view
	 * 
	 * @param model
	 * @return home page view
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Model model)
	{
		return "home";
	}

	/**
	 * Generates search page view
	 * 
	 * @param model
	 * @return search page view
	 */
	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public String search(Model model)
	{
		return "search";
	}

	/**
	 * Searches for tracks, artists & albums by pattern.
	 * 
	 * @param pattern search pattern
	 * @param model Spring Model - search results are stored here
	 * @return search page view
	 */
	@RequestMapping(value = "/search", method = RequestMethod.GET, params = "pattern")
	public String searchWithParams(@RequestParam String pattern, Model model)
	{
		if(pattern != null && pattern.trim().length() > 0)
		{
			List<Object[]> searchResults = trackService.getTracksByPattern(pattern);
			model.addAttribute("searchResults", searchResults);
		}

		return "search";
	}

	/**
	 * Generates browse page view.
	 * 
	 * @param model - Spring Model - stores track list here
	 * @return browse page view
	 */
	@RequestMapping(value = "/browse", method = RequestMethod.GET)
	public String browse(Model model)
	{
		List<Object[]> trackList = trackService.getAllTracks();
		model.addAttribute("trackList", trackList);
		return "browse";
	}

	/**
	 * Generates Top page view
	 * 
	 * @param model Spring Model - stores track top lists data here
	 * @return top page view
	 */
	@RequestMapping(value = "/top", method = RequestMethod.GET)
	public String top(Model model)
	{
		List<Object[]> topList = trackService.getTopTenTracks();
		model.addAttribute("topList", topList);
		return "top";
	}

	/**
	 * Generates track download
	 * 
	 * @param checksum MD5 checksum of the track
	 * @param response HttpServletResponse
	 * @return initiates track download and redirects to browse page
	 */
	@RequestMapping(value = "/download", method = RequestMethod.GET)
	public String getFile(@RequestParam("checksum") String checksum, HttpServletResponse response)
	{
		Track track = trackService.getTrackByChecksum(checksum);
		Download download = new Download(track);
		trackService.addDownload(download);

		String link = track.getLink();
		// To avoid any legal issues copyright protected material isn't being kept locally,
		// instead link to file will be replaced by a free sample audio file
		link = "audio.wma";

		// Forces the track to be downloaded, not played
		response.setContentType("application/download");
		response.setHeader("Content-Type", "audio/x-ms-wma");
		response.setHeader("Content-Disposition", "attachment; filename=" + link);

		// Generates file download dialog.
		try
		{
		    File audio = new File(link);
		    byte[] byteArray = new byte[(int) audio.length()];
		    FileInputStream inputStream = new FileInputStream(audio);
		    inputStream.read(byteArray);
		    ServletOutputStream outputStream = response.getOutputStream();
		    outputStream.write(byteArray);
		    outputStream.flush();
		    inputStream.close();
		}
		catch(IOException ioe) {  }

		return "redirect:/browse";
	}

}
