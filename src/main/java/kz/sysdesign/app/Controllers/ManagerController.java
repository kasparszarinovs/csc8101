package kz.sysdesign.app.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Manager section request controller.
 * Most requests made in this section are AJAX requests
 * and are parsed by AjaxController.
 * 
 * @author Kaspars Zarinovs &lt;k.zarinovs@ncl.ac.uk&gt;
 *
 */
@Controller
@RequestMapping(value="/manager")
public class ManagerController
{

	/**
	 * Generates view for manager section.
	 * 
	 * @param model
	 * @return manager view
	 */
	@RequestMapping(method=RequestMethod.GET)
	public String index(Model model)
	{
		return "manager";
	}

}
