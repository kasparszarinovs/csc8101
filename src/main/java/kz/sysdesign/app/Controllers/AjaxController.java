package kz.sysdesign.app.Controllers;

import java.util.List;

import kz.sysdesign.app.Helpers.RSError;
import kz.sysdesign.app.Services.AlbumService;
import kz.sysdesign.app.Services.ArtistService;
import kz.sysdesign.app.Services.TrackService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * AJAX requests controller.
 * 
 * Most methods use @ResponseBody annotation which tells
 * the method to return the value of the return
 * parameter instead of generating view.
 * 
 * @author Kaspars Zarinovs &lt;k.zarinovs@ncl.ac.uk&gt;
 *
 */
@Controller
@RequestMapping(value = "/ajax")
public class AjaxController {

	/**
	 *	Autowires artist service 
	 */
	@Autowired
	private ArtistService artistService;

	/**
	 * Autowires album service
	 */
	@Autowired
	private AlbumService albumService;

	/**
	 * Autowires track service
	 */
	@Autowired
	private TrackService trackService;

	/**
	 * Matches all artist names in the database to the pattern.
	 * Used to check whether such artist exists.
	 * 
	 * @param pattern artist name pattern
	 * @return artist names that match the pattern
	 */
	@RequestMapping(method=RequestMethod.GET, params = "matchingArtistsList")
	@ResponseBody
	public List<String> matchingArtists(@RequestParam String pattern) {
		List<String> responseList = artistService.getArtistNamesByPattern(pattern);
		return responseList;
	}

	/**
	 * Matches all album names in the database to the pattern.
	 * Used to check whether such album exists.
	 * 
	 * @param pattern album name pattern
	 * @return album names that match the pattern
	 */
	@RequestMapping(method=RequestMethod.GET, params = "matchingAlbumsList")
	@ResponseBody
	public List<String> matchingAlbums(@RequestParam String pattern) {
		List<String> responseList = albumService.getAlbumNamesByPattern(pattern);
		return responseList;
	}

	/**
	 * Adds artist to database.
	 * 
	 * @param artistName name of the artist
	 * @param modelMap Spring ModelMap
	 * @return database query state
	 */
	@RequestMapping(method = RequestMethod.POST, params = "submitArtist")
	@ResponseBody
	public ModelMap addArtist(@RequestParam("artistName") String artistName, ModelMap modelMap) 
	{
		if(artistName.trim().length() > 0) 
		{
			boolean artistCreated = artistService.addArtist(artistName);
			
			if(artistCreated)
				modelMap.addAttribute("success", artistCreated);
			else
				modelMap.addAttribute("error", "Artist already exists.");
		}
		else
			modelMap.addAttribute("error", RSError.getErrorMessage("emptyInputError").toString());
				
		return modelMap;
	}

	/**
	 * Adds album to database.
	 * 
	 * @param albumName name of the album
	 * @param albumArtist artist of the album
	 * @param yearString year of the album
	 * @param modelMap Spring ModelMap
	 * @return database query state
	 */
	@RequestMapping(method = RequestMethod.POST, params = "submitAlbum")
	@ResponseBody
	public ModelMap addAlbum(@RequestParam("albumName") String albumName,
							@RequestParam("albumArtist") String albumArtist,
							@RequestParam("albumYear") String yearString,
							ModelMap modelMap)
	{
		if(albumName.trim().length() > 0 && albumArtist.trim().length() > 0 && yearString.trim().length() > 0) 
		{
			try 
			{
				Integer year = Integer.parseInt(yearString);
				boolean albumCreated = albumService.addAlbum(albumName, albumArtist, year);
				
				if(albumCreated)
					modelMap.addAttribute("success", albumCreated);
				else
					modelMap.addAttribute("error", "Could not create a new album.");
			}
			catch (NumberFormatException e) 
			{
				modelMap.addAttribute("error", RSError.getErrorMessage("invalidInputError").toString());
			}
		} 
		else 
			modelMap.addAttribute("error", RSError.getErrorMessage("emptyInputError"));

		return modelMap;
	}

	/**
	 * Adds track to the database
	 * 
	 * @param trackName name of the track
	 * @param trackArtist artist of the track
	 * @param albumName album containing the track
	 * @param yearString year of the track
	 * @param modelMap Spring ModelMap
	 * @return database query state
	 */
	@RequestMapping(method = RequestMethod.POST, params = "submitTrack")
	@ResponseBody
	public ModelMap addTrack(@RequestParam("trackName") String trackName,
							@RequestParam("trackArtist") String trackArtist,
							@RequestParam(value="trackAlbum", required=false) String albumName,
							@RequestParam("trackYear") String yearString,
							ModelMap modelMap)
	{	
		if(trackName.trim().length() > 0 && trackArtist.trim().length() > 0 && yearString.trim().length() > 0) 
		{
			try 
			{
				Integer year = Integer.parseInt(yearString);
				boolean trackCreated = trackService.addTrack(trackName, trackArtist, albumName, year);
				
				if(trackCreated)
					modelMap.addAttribute("success", trackCreated);
				else
					modelMap.addAttribute("error", "Can't create a new track.");
			}
			catch (NumberFormatException e) 
			{
				modelMap.addAttribute("error", RSError.getErrorMessage("invalidInputError").toString());
			}
		} 
		else
			modelMap.addAttribute("error", RSError.getErrorMessage("emptyInputError").toString());

		return modelMap;
	}

}
