package kz.sysdesign.app.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

/**
 * Authorization requests controller.
 * 
 * @author Kaspars Zarinovs &lt;k.zarinovs@ncl.ac.uk&gt;
 *
 */
@Controller
public class AuthorizationController {

    /**
     * Generates login page view.
     * Redirects to root URL, if user is already logged in.
     * 
     * @param error error
     * @param model Spring Model
     * @param request HttpServletRequest
     * @return login view
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(@RequestParam(value="error", required = false) String error, Model model, HttpServletRequest request)
    {
        if(request.isUserInRole("ROLE_USER") || request.isUserInRole("ROLE_ADMIN"))
        	return "redirect:/";

        return "login";
    }

    /**
     * Generates login page view & triggers displaying of 
     * an error message in the view.
     * Redirects to root URL, if user is already logged in.
     * 
     * @param error error
     * @param model Spring Model
     * @param request HttpServletRequest
     * @return login view
     */
    @RequestMapping(value="/login", params = "error")
    public String loginError(@RequestParam(value="error") String error, Model model, HttpServletRequest request)
    {
        if(request.isUserInRole("ROLE_USER") || request.isUserInRole("ROLE_ADMIN"))
        	return "redirect:/";

        model.addAttribute("error", true);
        return "login";
    }

    /**
     * Generates view for "unauthorized" page
     * 
     * @return unauthorized view
     */
    @RequestMapping(value="/unauthorized", method = RequestMethod.GET)
    public String unauthorized()
    {
        return "unauthorized";
    }

    /*
    @RequestMapping(method = RequestMethod.POST, params = "submitUser")
    public String addUser(@RequestParam("userName") String userName,
    						@RequestParam("userEmail") String userEmail,
    						@RequestParam("userPassword") String userPassword,
    						Model model) {

    	if(userName.trim().length() > 0
    			&& userEmail.trim().length() > 0
    			&& userPassword.trim().length() > 0) {

    		StoreUser createdUser = storeUserService.addUser(userName, userEmail, userPassword);
    		model.addAttribute("createUserError", createdUser == null);

    	} else
    		model.addAttribute("emptyInputError", true);

    	return "signup";

    }
    */
}
