package kz.sysdesign.app.Controllers;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kz.sysdesign.app.Entities.*;
import kz.sysdesign.app.Services.AlbumService;
import kz.sysdesign.app.Services.ArtistService;
import kz.sysdesign.app.Services.TrackService;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

/**
 * RDF controller.
 * 
 * @author Kaspars Zarinovs &lt;k.zarinovs@ncl.ac.uk&gt;
 *
 */
@Controller
public class RdfController {

	/**
	 * Autowires track service
	 */
	@Autowired
	private TrackService trackService;

	/**
	 * Autowires album service
	 */
	@Autowired
	private AlbumService albumService;

	/**
	 * Autowires artist service
	 */
	@Autowired
	private ArtistService artistService;

	/**
	 * @param model Spring Model
	 * @param response HttpServletResponse
	 * @return generates RDF schema of the database contents
	 */
	@RequestMapping(value = "/rdf", method = RequestMethod.GET)
	public String rdfGenerator(Model model, HttpServletResponse response)
	{
		try
		{	
		    List<Track> tracksList = trackService.getAllTracksList();
		    List<Artist> artistsList = artistService.getAllArtists();
		    List<Album> albumsList = albumService.getAllAlbums();

		    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		    dbFactory.setNamespaceAware(true);
		    DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();
		    DOMImplementation domImpl = docBuilder.getDOMImplementation();

		    Document doc = domImpl.createDocument("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "rdf:RDF", null);

		    Element root = doc.getDocumentElement();
		    
		    root.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:track", "http://csc8101-store.org/track#");
		    root.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:artist", "http://csc8101-store.org/artist#");
		    root.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:album", "http://csc8101-store.org/album#");

		    for(Track track : tracksList)
		    {
		        Element firstChild = doc.createElement("rdf:Description");
		        
		        firstChild.setAttribute("rdf:about", "http://csc8101-store.org/track/"+track.getName());
		        root.appendChild(firstChild);
		        
		        Element artist = doc.createElement("track:artist");
		        root.appendChild(artist);

		        Element albums = doc.createElement("track:albums");
		        
		        for(Album album : track.getAlbums())
		        {
		        	albums.setAttribute("rdf:resource", "http://csc8101-store.org/track/"+album.getName());
		            root.appendChild(albums);
		        }
		        
		        Element year = doc.createElement("track:year");
		        root.appendChild(year);

		        Element link = doc.createElement("track:link");
		        root.appendChild(link);
		        
		        Element md5checksum = doc.createElement("track:md5checksum");
		        root.appendChild(md5checksum);

		        Text text = doc.createTextNode(track.getYear().toString());
		        year.appendChild(text);

		        text = doc.createTextNode(track.getLink());
		        link.appendChild(text);
		        
		        text = doc.createTextNode(track.getArtist().getName());
		        albums.appendChild(text);
		    }
		    
		    for(Artist artist : artistsList)
		    {
		        Element firstChild = doc.createElement("rdf:Description");
		        firstChild.setAttribute("rdf:about", "http://csc8101-store.org/artist/"+artist.getName());
		        root.appendChild(firstChild);

		        Element name = doc.createElement("artist:name");
		        root.appendChild(name);
		        
		        Element albums = doc.createElement("artist:albums");
		        for(Album album : artist.getAlbums())
		        {
		        	albums.setAttribute("rdf:resource", "http://csc8101-store.org/artist/"+album.getName());
		            root.appendChild(albums);
		        }

		        Element tracks = doc.createElement("artist:tracks");
		        for(Track track : artist.getTracks())
		        {
		        	tracks.setAttribute("rdf:resource", "http://csc8101-store.org/artist/"+track.getName());
		            root.appendChild(tracks);
		        }

		        Text text = doc.createTextNode(artist.getName());
		        name.appendChild(text);
		    }

		    for(Album album : albumsList)
		    {
		        Element firstChild = doc.createElement("rdf:Description");
		        firstChild.setAttribute("rdf:about", "http://csc8101-store.org/album/"+album.getName());
		        root.appendChild(firstChild);

		        Element name = doc.createElement("album:name");
		        root.appendChild(name);
		        
		        Element artist = doc.createElement("album:artist");
		        root.appendChild(artist);

		        Element tracks = doc.createElement("album:tracks");
		        for(Track track : album.getTracks())
		        {
		        	tracks.setAttribute("rdf:resource", "http://csc8101-store.org/album/"+track.getName());
		            root.appendChild(tracks);
		        }
		        
		        Element year = doc.createElement("album:year");
		        root.appendChild(year);

		        Text text = doc.createTextNode(album.getName());
		        name.appendChild(text);
		        
		        text = doc.createTextNode(album.getArtist().getName());
		        name.appendChild(text);
		        
		        text = doc.createTextNode(album.getYear().toString());
		        year.appendChild(text);
		    }	    
		    	
		    TransformerFactory transfac = TransformerFactory.newInstance();
		    Transformer trans = transfac.newTransformer();
		    trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		    trans.setOutputProperty(OutputKeys.INDENT, "yes");


		    StringWriter sw = new StringWriter();
		    StreamResult result = new StreamResult(sw);
		    DOMSource source = new DOMSource(doc);
		    trans.transform(source, result);
		    String xmlString = sw.toString();
		    
		    response.setContentType("application/download");
			response.setHeader("Content-Type", "text/xml");
			response.setHeader("Content-Disposition", "attachment; filename=RDF.xml");
			
			ServletOutputStream outputStream = response.getOutputStream();
			outputStream.println(xmlString);
			outputStream.flush();
			
			model.addAttribute("rdf", xmlString);
		}
		catch(Exception e) { e.printStackTrace(); }

		return "redirect:/";
	}

}
