package kz.sysdesign.app.Entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Store user entity.
 * 
 * @author Kaspars Zarinovs &lt;k.zarinovs@ncl.ac.uk&gt;
 *
 */
@Entity
public class StoreUser implements Serializable {

	private static final long serialVersionUID = -1275385405481463748L;
	private Long id;
	private String name;
	private String email;
	private String password;
	private String salt;

	/**
	 * Default constructor
	 */
	public StoreUser() {
		
	}

	/**
	 * @param name username
	 * @param email user's email address
	 * @param password hashed password
	 * @param salt salt used to hash password
	 */
	public StoreUser(String name, String email, String password, String salt) {
		this.name = name;
		this.email = email;
		this.password = password;
		this.salt = salt;
	}

	/**
	 * @return user id
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	/**
	 * @param id user id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return username
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name username
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Has to be unique.
	 * 
	 * @return user's email address
	 */
	@Column(unique=true)
	public String getEmail() {
		return email;
	}

	/**
	 * @param email user's email address
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return hashed password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password hashed password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return salt used to hash the password
	 */
	public String getSalt() {
		return salt;
	}

	/**
	 * @param salt salt used to hash the password
	 */
	public void setSalt(String salt) {
		this.salt = salt;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return email + " " + name;
	}

}
