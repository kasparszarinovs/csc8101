package kz.sysdesign.app.Entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import kz.sysdesign.app.Helpers.RSHash;

/**
 * Album entity.
 * 
 * @author Kaspars Zarinovs &lt;k.zarinovs@ncl.ac.uk&gt;
 *
 */
@Entity
public class Album implements Serializable {

	private static final long serialVersionUID = 2923299524115034893L;
	private Long id;
	private String name;
	private Artist artist;
	private Integer year;
	private List<Track> tracks;
	private String md5checksum;

	/**
	 * Default constructor.
	 */
	public Album() {
		
	}

	/**
	 * @param name name of the album
	 * @param artist album's artist
	 * @param year album's production year
	 */
	public Album(String name, Artist artist, int year) {
		this.name = name;
		this.artist = artist;
		this.year = year;
		this.md5checksum = RSHash.generateMD5Hash(name.toLowerCase() + artist.getId() + year);
	}

	/**
	 * @return album's id
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	/**
	 * @param id Long album's id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return name of the album
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name String name of the album
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return artist of the album
	 */
	@ManyToOne
	public Artist getArtist() {
		return artist;
	}

	/**
	 * @param artist artist of the album
	 */
	public void setArtist(Artist artist) {
		this.artist = artist;
	}

	/**
	 * @return year album was produced
	 */
	public Integer getYear() {
		return year;
	}

	/**
	 * @param year year album was produced
	 */
	public void setYear(Integer year) {
		this.year = year;
	}

	/**
	 * @return list of tracks in the album
	 */
	@ManyToMany
	@Column(nullable=true)
	public List<Track> getTracks() {
		return tracks;
	}

	/**
	 * @param tracks list of tracks in the album
	 */
	public void setTracks(List<Track> tracks) {
		this.tracks = tracks;
	}

	/**
	 * @return unique MD5 checksum
	 */
	@Column(unique=true)
	public String getMd5checksum() {
		return md5checksum;
	}

	/**
	 * @param md5checksum MD5 checksum
	 */
	public void setMd5checksum(String md5checksum) {
		this.md5checksum = md5checksum;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return name;
	}

}
