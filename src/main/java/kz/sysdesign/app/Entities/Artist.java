package kz.sysdesign.app.Entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * Artist entity.
 * 
 * @author Kaspars Zarinovs &lt;k.zarinovs@ncl.ac.uk&gt;
 *
 */
@Entity
public class Artist implements Serializable {

	private static final long serialVersionUID = -4077307699646683610L;
	private Long id;
	private String name;
	private List<Album> albums;
	private List<Track> tracks;

	/**
	 * Default constructor
	 */
	public Artist() {
		
	}

	/**
	 * @param name the artist's name
	 */
	public Artist(String name) {
		this.setName(name);
	}

	/**
	 * @return artist's id
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	/**
	 * @param id artist's id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return artist's name
	 */
	@Column(unique=true)
	public String getName() {
		return name;
	}

	/**
	 * @param name artist's name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return list of artists's albums
	 */
	@OneToMany(cascade = CascadeType.REMOVE)
	@Column(nullable=true)
	public List<Album> getAlbums() {
		return albums;
	}

	/**
	 * @param albums list of albums
	 */
	public void setAlbums(List<Album> albums) {
		this.albums = albums;
	}

	/**
	 * @return list of artist's tracks
	 */
	@OneToMany(cascade = CascadeType.REMOVE)
	@Column(nullable=true)
	public List<Track> getTracks() {
		return tracks;
	}

	/**
	 * @param tracks list of tracks
	 */
	public void setTracks(List<Track> tracks) {
		this.tracks = tracks;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString () {
		return name;
	}

}
