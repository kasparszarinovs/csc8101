package kz.sysdesign.app.Entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import kz.sysdesign.app.Helpers.RSHash;

/**
 * Track entity.
 * 
 * @author Kaspars Zarinovs &lt;k.zarinovs@ncl.ac.uk&gt;
 *
 */
@Entity
public class Track implements Serializable {

	private static final long serialVersionUID = 9074603540193021226L;
	private Long id;
	private String name;
	private Artist artist;
	private List<Album> albums;
	private Integer year;
	private String link;
	private String md5checksum;
	private List<Download> listOfDownloads;

	/**
	 * Default constructor.
	 */
	public Track() {
		
	}

	/**
	 * @param name track name
	 * @param artist artist of the track
	 * @param albums album that contains the track
	 * @param year year produced
	 */
	public Track(String name, Artist artist, List<Album> albums, int year) {
		this.name = name;
		this.artist = artist;
		this.albums = albums;
		this.year = year;
		this.md5checksum = RSHash.generateMD5Hash(name.toLowerCase() + artist.getId() + year);
		this.link = "/downloads/" +  md5checksum.substring(0, 3) + "-" + artist.getName() + "-" + name + ".mp3";
	}

	/**
	 * @return track id
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	/**
	 * @param id track id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return track name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name track name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return artist of the track
	 */
	@ManyToOne
	public Artist getArtist() {
		return artist;
	}

	/**
	 * @param artist artist of the track
	 */
	public void setArtist(Artist artist) {
		this.artist = artist;
	}

	/**
	 * @return list of albums that contain the track
	 */
	@ManyToMany
	@Column(nullable = true)
	public List<Album> getAlbums() {
		return albums;
	}

	/**
	 * @param albums list of albums
	 */
	public void setAlbums(List<Album> albums) {
		this.albums = albums;
	}

	/**
	 * @return the year track was produced
	 */
	public Integer getYear() {
		return year;
	}

	/**
	 * @param the year track was produced
	 */
	public void setYear(Integer year) {
		this.year = year;
	}

	/**
	 * @return unique MD5 checksum of the track
	 */
	@Column(unique = true)
	public String getMd5checksum() {
		return md5checksum;
	}

	/**
	 * @param md5checksum MD5 checksum
	 */
	public void setMd5checksum(String md5checksum) {
		this.md5checksum = md5checksum;
	}

	/**
	 * @return direct link to track
	 */
	@Column(unique = true)
	public String getLink() {
	    return link;
	}

	/**
	 * @param link direct link to track
	 */
	public void setLink(String link) {
	    this.link = link;
	}

	/**
	 * @return list of track downloads
	 */
	@OneToMany(cascade = CascadeType.REMOVE)
	public List<Download> getListOfDownloads() {
	    return listOfDownloads;
	}

	/**
	 * @param listOfDownloads list of track downloads
	 */
	public void setListOfDownloads(List<Download> listOfDownloads) {
	    this.listOfDownloads = listOfDownloads;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return name;
	}

}
