package kz.sysdesign.app.Entities;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Date;

/**
 * Download entity.
 * 
 * @author Kaspars Zarinovs &lt;k.zarinovs@ncl.ac.uk&gt;
 *
 */
@Entity
public class Download implements Serializable {

    private Long id;
    private Track track;
    private Date timestamp;

    /**
     * Default constructor.
     */
    public Download() {
    	
    }

    /**
     * @param track the track
     */
    public Download(Track track) {
        this.track = track;
        this.timestamp = new Date();
    }

    /**
     * @return download's id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    /**
     * @param id download's id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return track
     */
    @ManyToOne
    public Track getTrack() {
        return track;
    }

    /**
     * @param track the track
     */
    public void setTrack(Track track) {
        this.track = track;
    }

    /**
     * @return timestamp when track was added
     */
    @Temporal(TemporalType.DATE)
    public Date getTimestamp() {
        return timestamp;
    }

    /**
     * @param timestamp the timestamp
     */
    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

}
