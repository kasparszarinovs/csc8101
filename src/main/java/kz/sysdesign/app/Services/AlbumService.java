package kz.sysdesign.app.Services;

import java.util.List;

import kz.sysdesign.app.Entities.Album;

/**
 * Album service.
 * Contains business logic for album service & calls 
 * DAO classes to interact with the database.
 * 
 * @author Kaspars Zarinovs &lt;k.zarinovs@ncl.ac.uk&gt;
 *
 */
public interface AlbumService {
	
	/**
	 * Validates album data and uses album DAO to add it to database.
	 * 
	 * @param albumName name of the album
	 * @param artistName name of the album artist
	 * @param year the year album was produced
	 * @return <code>true</code> if successful, <code>false</code> if unsuccessful
	 */
	public boolean addAlbum(String albumName, String artistName, int year);
	
	/**
	 * Validates the pattern and uses DAO to get the album names.
	 * 
	 * @param pattern the pattern to match album names
	 * @return list of albums that match the pattern
	 */
	public List<String> getAlbumNamesByPattern(String pattern);
	
	/**
	 * Uses album DAO to return all albums.
	 * 
	 * @return list of albums
	 */
	public List<Album> getAllAlbums();
	
}
