package kz.sysdesign.app.Services;

import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;

import kz.sysdesign.app.DAO.StoreUserDAO;
import kz.sysdesign.app.Entities.StoreUser;
import kz.sysdesign.app.Helpers.RSHash;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Store user service implementation.
 * 
 * @author Kaspars Zarinovs &lt;k.zarinovs@ncl.ac.uk&gt;
 *
 */
@Service
public class StoreUserServiceImpl implements StoreUserService {

	/**
	 * Autowires store user DAO.
	 */
	@Autowired
	private StoreUserDAO storeUserDAO;

	/**
	 * @see kz.sysdesign.app.Services.StoreUserService#addUser(java.lang.String, java.lang.String, java.lang.String)
	 */
	public StoreUser addUser(String name, String email, String password)
	{
		if(name == null || email == null || password == null)
			return null;
		
		StoreUser storeUser = null;
		try 
		{
			String salt = RSHash.generateSalt();
			String hashable = password + salt;
			StoreUser tempStoreUser = new StoreUser(name, email, RSHash.generateSHA256Hash(hashable), salt);
			
			storeUserDAO.addUser(tempStoreUser);
			storeUser = tempStoreUser;
		}
		catch(RuntimeException e) {  }
		
		return storeUser;
	}

	/**
	 * @see kz.sysdesign.app.Services.StoreUserService#authenticate(java.lang.String, java.lang.String)
	 */
	public StoreUser authenticate(String loginName, String loginPassword)
	{
		if(loginName == null || loginPassword == null)
			return null;
		
		StoreUser user = null;
		try
		{
			String salt = storeUserDAO.getSalt(loginName);
			System.out.println(RSHash.generateSHA256Hash(loginPassword + salt));
			user = (StoreUser) storeUserDAO.authenticate(loginName, RSHash.generateSHA256Hash(loginPassword + salt));
		}
		catch(NonUniqueResultException ue) { }
		catch(NoResultException e) { }
		
		return user;
	}

}
