package kz.sysdesign.app.Services;

import kz.sysdesign.app.Entities.StoreUser;

/**
 * Store user service interface.
 * Manages business logic for store users and uses
 * DAOs to interact with the database.
 * 
 * @author Kaspars Zarinovs &lt;k.zarinovs@ncl.ac.uk&gt;
 *
 */
public interface StoreUserService {

	    /**
	     * @param name username
	     * @param email user's email address
	     * @param password password
	     * @return <code>StoreUser</code> object if successful, <code>null</code> if unsuccessful
	     */
	    public StoreUser addUser(String name, String email, String password);
	    
	    /**
	     * @param loginName username
	     * @param loginPassword password
	     * @return <code>StoreUser</code> object if successful, <code>null</code> if unsuccessful
	     */
	    public StoreUser authenticate(String loginName, String loginPassword);
	
}
