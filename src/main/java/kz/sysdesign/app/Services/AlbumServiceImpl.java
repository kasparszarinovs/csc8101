package kz.sysdesign.app.Services;

import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;

import kz.sysdesign.app.DAO.AlbumDAO;
import kz.sysdesign.app.DAO.ArtistDAO;
import kz.sysdesign.app.Entities.Album;
import kz.sysdesign.app.Entities.Artist;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implementation of the album service.
 * 
 * @author Kaspars Zarinovs &lt;k.zarinovs@ncl.ac.uk&gt;
 *
 */
@Service
public class AlbumServiceImpl implements AlbumService {

	// Logger
	// private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	// Usage: logger.info("info");

	/**
	 * Autowires album DAO.
	 */
	@Autowired
	AlbumDAO albumDAO;

	/**
	 * Autowires artist DAO.
	 */
	@Autowired
	ArtistDAO artistDAO;

	/**
	 * @see kz.sysdesign.app.Services.AlbumService#addAlbum(java.lang.String, java.lang.String, int)
	 */
	public boolean addAlbum(String albumName, String artistName, int year) 
	{
		if(albumName == null || artistName == null)
			return false;

		boolean albumCreated = false;
		try 
		{
			Artist artist = artistDAO.getArtistByName(artistName);
			Album album = new Album(albumName, artist, year);
			albumCreated = albumDAO.addAlbum(album);
		}
		catch(NonUniqueResultException nue) {  }
		catch(NoResultException nre) {  }
		catch(RuntimeException e) {  }

		return albumCreated;
	}

	/**
	 * @see kz.sysdesign.app.Services.AlbumService#getAlbumNamesByPattern(java.lang.String)
	 */
	public List<String> getAlbumNamesByPattern(String pattern) 
	{
		if(pattern.trim().length() == 0 || pattern == null) 
			return null;

		return albumDAO.getAlbumNamesByPattern(pattern);
	}

	/**
	 * @see kz.sysdesign.app.Services.AlbumService#getAllAlbums()
	 */
	public List<Album> getAllAlbums() 
	{
		return albumDAO.getAllAlbums();
	}

}
