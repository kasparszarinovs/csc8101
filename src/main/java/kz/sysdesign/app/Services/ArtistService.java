package kz.sysdesign.app.Services;

import java.util.List;

import kz.sysdesign.app.Entities.Artist;

/**
 * Artist service interface.
 * Contains business logic for artist service & calls 
 * DAO classes to interact with the database.
 * 
 * @author Kaspars Zarinovs &lt;k.zarinovs@ncl.ac.uk&gt;
 *
 */
public interface ArtistService {

	/**
	 * Validates artist data and uses artist DAO
	 * to store a new artist in database.
	 * 
	 * @param artistName
	 * @return <code>true</code> if successful, <code>false</code> if unsuccessful
	 */
	public boolean addArtist(String artistName);
	
	/**
	 * Returns list of artists which match the pattern
	 * 
	 * @param pattern the pattern to match artist to
	 * @return list of artists
	 */
	public List<String> getArtistNamesByPattern(String pattern);
	
	/**
	 * Returns list of all artists.
	 * 
	 * @return list of artists
	 */
	public List<Artist> getAllArtists();
	
}
