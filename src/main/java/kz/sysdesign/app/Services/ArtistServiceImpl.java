package kz.sysdesign.app.Services;

import java.util.List;

import kz.sysdesign.app.DAO.ArtistDAO;
import kz.sysdesign.app.Entities.Artist;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Artist service implementation.
 * 
 * @author Kaspars Zarinovs &lt;k.zarinovs@ncl.ac.uk&gt;
 *
 */
@Service
public class ArtistServiceImpl implements ArtistService {

	// Logger
	// private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	// Usage: logger.info("info");

	/**
	 * Autowire artist DAO.
	 */
	@Autowired
	ArtistDAO artistDAO;

	/**
	 * @see kz.sysdesign.app.Services.ArtistService#addArtist(java.lang.String)
	 */
	public boolean addArtist(String artistName)
	{
		if(artistName == null)
			return false;

		boolean artistCreated = false;
		try
		{
			Artist tempArtist = new Artist(artistName);
			artistCreated = artistDAO.addArtist(tempArtist);
		}
		catch(RuntimeException e) {  }

		return artistCreated;
	}

	/**
	 * @see kz.sysdesign.app.Services.ArtistService#getArtistNamesByPattern(java.lang.String)
	 */
	public List<String> getArtistNamesByPattern(String pattern) 
	{
		if(pattern.trim().length() == 0 || pattern == null)
			return null;
		
		return artistDAO.getArtistNamesByPattern(pattern);
	}

	/**
	 * @see kz.sysdesign.app.Services.ArtistService#getAllArtists()
	 */
	public List<Artist> getAllArtists()
	{
		return artistDAO.getAllArtists();
	}

}
