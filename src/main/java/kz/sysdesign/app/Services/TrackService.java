package kz.sysdesign.app.Services;

import kz.sysdesign.app.Entities.Download;
import kz.sysdesign.app.Entities.Track;

import java.util.List;

/**
 * Track service interface.
 * 
 * @author Kaspars Zarinovs &lt;k.zarinovs@ncl.ac.uk&gt;
 *
 */
public interface TrackService {
	
	/**
	 * Validates track data and uses track DAO to add
	 * the track to the database.
	 * 
	 * @param trackName track name
	 * @param artistName artist name
	 * @param albumName album name
	 * @param year the year track produced
	 * @return <code>true</code> if successful, <code>null</code> if unsuccessful
	 */
	public boolean addTrack(String trackName, String artistName, String albumName, int year);
	
	/**
	 * Returns list of tracks that matches the pattern.
	 * 
	 * @param pattern the pattern to match tracks to
	 * @return list of tracks that matches the pattern
	 */
	public List<Object[]> getTracksByPattern(String pattern);
	
	/**
	 * Uses track DAO to return all tracks from the database.
	 * 
	 * @return list of tracks
	 */
	public List<Object[]> getAllTracks();

    /**
     * Gets top 10 tracks from the database, by using track DAO.
     * 
     * @return list of top 10 tracks
     */
    public List<Object[]> getTopTenTracks();

    /**
     * Gets track by it's MD5 checksum.
     * 
     * @param checksum MD5 checksum of the track
     * @return track
     */
    public Track getTrackByChecksum(String checksum);

    /**
     * Uses track DAO to add a download to database.
     * 
     * @param download download object
     * @return <code>true</code> if successful, <code>null</code> if unsuccessful
     */
    public boolean addDownload(Download download);
    
    /**
     * Uses track DAO to get all tracks from database.
     * 
     * @return list of tracks
     */
    public List<Track> getAllTracksList();
	
}
