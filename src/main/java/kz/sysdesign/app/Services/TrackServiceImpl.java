package kz.sysdesign.app.Services;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;

import kz.sysdesign.app.Entities.Download;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kz.sysdesign.app.DAO.AlbumDAO;
import kz.sysdesign.app.DAO.ArtistDAO;
import kz.sysdesign.app.DAO.TrackDAO;
import kz.sysdesign.app.Entities.Album;
import kz.sysdesign.app.Entities.Artist;
import kz.sysdesign.app.Entities.Track;

/**
 * Track service implementation.
 * 
 * @author Kaspars Zarinovs &lt;k.zarinovs@ncl.ac.uk&gt;
 *
 */
@Service
public class TrackServiceImpl implements TrackService {

	// Logger
	// private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	// Usage: logger.info("info");

	/**
	 * Autowire track DAO.
	 */
	@Autowired
	TrackDAO trackDAO;

	/**
	 * Autowire artist DAO.
	 */
	@Autowired
	ArtistDAO artistDAO;

	/**
	 * Autowire album DAO.
	 */
	@Autowired
	AlbumDAO albumDAO;

	/**
	 * @see kz.sysdesign.app.Services.TrackService#addTrack(java.lang.String, java.lang.String, java.lang.String, int)
	 */
	public boolean addTrack(String trackName, String artistName, String albumName, int year) 
	{
		if(trackName == null || artistName == null || albumName == null)
			return false;

		boolean trackCreated = false;
		try 
		{
			Artist artist = artistDAO.getArtistByName(artistName);
			List <Album> albumList = null;
			if(albumName != null) 
			{
				Album album = albumDAO.getAlbumByName(albumName);
				albumList = new ArrayList<Album>();
				albumList.add(album);
			}
			Track track = new Track(trackName, artist, albumList, year);
			trackCreated = trackDAO.addTrack(track);
		}
		catch(NonUniqueResultException ue) {  }
		catch(NoResultException ne) {  }
		catch(RuntimeException re) {  }

		return trackCreated;
	}

	/**
	 * @see kz.sysdesign.app.Services.TrackService#getTracksByPattern(java.lang.String)
	 */
	public List<Object[]> getTracksByPattern(String pattern) 
	{
		if(pattern == null) 
			return null;

		return trackDAO.getTracksByPattern(pattern);
	}

	/**
	 * @see kz.sysdesign.app.Services.TrackService#getAllTracks()
	 */
	public List<Object[]> getAllTracks() 
	{
		return trackDAO.getAllTracks();
	}

	/**
	 * @see kz.sysdesign.app.Services.TrackService#getTopTenTracks()
	 */
	public List<Object[]> getTopTenTracks() 
	{
		return trackDAO.getTopTenTracks();
	}

	/**
	 * @see kz.sysdesign.app.Services.TrackService#getTrackByChecksum(java.lang.String)
	 */
	public Track getTrackByChecksum(String checksum) 
	{
		if(checksum == null) 
			return null;

		return trackDAO.getTrackByChecksum(checksum);
	}

	/**
	 * @see kz.sysdesign.app.Services.TrackService#addDownload(kz.sysdesign.app.Entities.Download)
	 */
	public boolean addDownload(Download download) 
	{
		if(download == null) 
			return false;

		boolean downloadAdded = false;
		try 
		{
			downloadAdded = trackDAO.addDownload(download);
		}
		catch(RuntimeException re) {  }

		return downloadAdded;
	}

	/**
	 * @see kz.sysdesign.app.Services.TrackService#getAllTracksList()
	 */
	public List<Track> getAllTracksList() 
	{
		return trackDAO.getAllTracksList();
	}

}
