(function(window,$) {
	
$(function() {
	
	/* Tablesorter for Search & Browse */
	$('table#results').tablesorter();
	
	/* Store Manager */
	if($('.manager').length > 0) {
		
		var cache = { artist: {}, album: {} },
			$managerContents = $('#content'),
			$managerTabs = $managerContents.find('.manager-tab'),
			$managerMenu = $('#manager-menu'),
			$managerTabTriggers = $managerMenu.find('.manager-tab-trigger'),
			wlh = window.location.hash;
		
		$('.artistName').autocomplete({
			minLength: 2,
            source: function(req, resp) {
            	var term = req.term;
            	if(term in cache.artist) {
            		resp(cache.artist[term]);
            		return;
            	}
            	
            	$.getJSON('ajax', { pattern: term, matchingArtistsList: true }, function(data) {
            		cache.artist[term] = data;
    				resp(data);
    			});
            }
        });
		
		$('.albumName').autocomplete({
			minLength: 2,
            source: function(req, resp) {
            	var term = req.term;
            	if(term in cache.album) {
            		resp(cache.album[term]);
            		return;
            	}
            	
            	$.getJSON('ajax', { pattern: term, matchingAlbumsList: true }, function(data) {
            		cache.album[term] = data;
    				resp(data);
    			});
            }
		});

		if(wlh && $.inArray(wlh, [ '#tab-artist', '#tab-album', '#tab-track' ]) > -1) {
			$managerTabs.removeClass('active').hide();
			$managerTabTriggers.removeClass('active');
			$(wlh).addClass('active').show();
			$(wlh + '-trigger').addClass('active');
		} else {
			window.location.hash = $managerMenu.find('.active').attr('href');
		}
			
		$managerTabTriggers.on('click', function() {
			var $this = $(this),
				$messages = $managerTabs.find('.messages'),
				$activeTrigger = $managerMenu.find('.active'),
				$activeTab = $managerContents.find('.active'),
				$tab = $($this.attr('href'));
			
			$activeTrigger.removeClass('active');
			$this.addClass('active');
			
			$activeTab.removeClass('active').fadeOut(200, function() {
				if($this.attr('id') != $activeTrigger.attr('id'))
					$messages.empty();
				
				window.location.hash = $this.attr('href');
				$tab.addClass('active').fadeIn(200);
			});
			
			return false;
		});
		
		$managerContents.find('form').on('submit', function() {
			var $this = $(this),
				$messages = $this.find('.messages');
			
			$messages.empty();
			
			$.post('ajax', $this.find('input').serialize(), function(data) {				
				if(data['success']) {
					$messages.append('<p class="success">Success!</p>');
					$this.find('input[type="text"]').val('');
				}
				
				if(data['error']) {
					$messages.append('<p class="error">' + data.error + '</p>');
				}
			});
			
			return false;
		});
	}
	
});
	
})(window,jQuery);