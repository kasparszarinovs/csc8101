<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<c:url var="resourcesRoot" value="/resources/" />
<c:url var="siteRoot" value="/" />
<c:set var="authorized" value="false" />
<c:set var="isAdmin" value="false" />
<sec:authorize access="isAuthenticated()"><c:set var="authorized" value="true" /></sec:authorize>
<sec:authorize access="hasRole('ROLE_ADMIN')"><c:set var="isAdmin" value="true" /></sec:authorize>

<!doctype html>
<html>
<head>
	<title>Home</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	
	<link rel="stylesheet" href="${resourcesRoot}css/normalize.css" type="text/css" />
	<link rel="stylesheet" href="${resourcesRoot}css/jqui/jquery-ui-1.9.2.custom.min.css" type="text/css" />
	<link rel="stylesheet" href="${resourcesRoot}css/style.css" type="text/css" />
</head>
<body>
<div class="wrapper">
	<div class="container">
		<div class="title">Music store</div>
	
		<header>
			<nav class="cf">
				<ul class="main-menu">
					<li><a href="${siteRoot}" class="active" title="Home">Home</a>
					<li><a href="${siteRoot}search" title="Search">Search</a>
					<li><a href="${siteRoot}browse" title="Browse tracks, artists & albums">Browse</a>
					<li><a href="${siteRoot}top" title="Weekly top">Top 10</a>
					<li><a href="${siteRoot}resources/RDFS.xml">RDFS</a>
				</ul>
				<ul class="secondary-menu">
                    <c:if test="${isAdmin}"><li><a href="${siteRoot}manager" title="Manager">Store manager</a></c:if>
					<c:choose>
                        <c:when test="${authorized}"><li><a href="${siteRoot}logout">Logout</a></li></c:when>
                        <c:otherwise><li><a href="${siteRoot}login" title="Login">Login</a></li></c:otherwise>
					</c:choose>
				</ul>
			</nav>
		</header>
		
		<div class="content home">
		<h1>Search our track library</h1>
		<form method="get" action="search">
			<input type="text" name="pattern" placeholder="Search for track, artist or album..." />
			<input type="submit" value="Search" />
		</form>
		</div>
		
	</div>
</div>
<script type="text/javascript" src="${resourcesRoot}js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="${resourcesRoot}js/jquery-ui-1.9.2.custom.min.js"></script>
<script type="text/javascript" src="${resourcesRoot}js/jquery.tablesorter.min.js"></script>
<script type="text/javascript" src="${resourcesRoot}js/script.js"></script>
</body>
</html>