<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<c:url var="resourcesRoot" value="/resources/" />
<c:url var="siteRoot" value="/" />

<!doctype html>
<html>
<head>
	<title>Signup</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	
	<link rel="stylesheet" href="${resourcesRoot}/css/normalize.css" type="text/css" />
	<link rel="stylesheet" href="${resourcesRoot}/css/jqui/jquery-ui-1.9.2.custom.min.css" type="text/css" />
	<link rel="stylesheet" href="${resourcesRoot}/css/style.css" type="text/css" />
</head>
<body>
<div class="login signup">
<c:if test="${errorState}">
	<div class="error">${error}</div>
</c:if>
<c:if test="${createUserError}">
	<div class="error">Uh oh... could not create a new user.</div>
</c:if>

	<!-- StoreUser -->
	<div class=content>
	<form method="post">
		<fieldset>
			<label for="userEmail">E-mail</label>
			<input type="email" name="userEmail" id="userEmail" required />
		</fieldset>
		<fieldset>
			<label for="userName">Name</label>
			<input type="text" name="userName" id="userName" required />
		</fieldset>
		<fieldset>
			<label for="userPassword">Password</label>
			<input type="password" name="userPassword" id="userPassword" required />
		</fieldset>
		<fieldset>
			<input type="submit" name="submitUser" value="Sign up" />
		</fieldset>
	</form>
	</div>

	<div class="info">Already registered? <a href="${siteRoot}" title="Sign in">Sign in</a></div>
	
</div>

<script type="text/javascript" src="${resourcesRoot}js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="${resourcesRoot}js/jquery-ui-1.9.2.custom.min.js"></script>
<script type="text/javascript" src="${resourcesRoot}js/jquery.tablesorter.min.js"></script>
<script type="text/javascript" src="${resourcesRoot}js/script.js"></script>

</body>
</html>