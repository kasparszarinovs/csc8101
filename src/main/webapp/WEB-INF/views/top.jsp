<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<c:url var="resourcesRoot" value="/resources/" />
<c:url var="siteRoot" value="/" />
<c:set var="authorized" value="false" />
<c:set var="isAdmin" value="false" />
<sec:authorize access="isAuthenticated()"><c:set var="authorized" value="true" /></sec:authorize>
<sec:authorize access="hasRole('ROLE_ADMIN')"><c:set var="isAdmin" value="true" /></sec:authorize>

<!doctype html>
<html>
<head>
    <title>Search</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="stylesheet" href="${resourcesRoot}css/normalize.css" type="text/css" />
    <link rel="stylesheet" href="${resourcesRoot}css/jqui/jquery-ui-1.9.2.custom.min.css" type="text/css" />
    <link rel="stylesheet" href="${resourcesRoot}css/style.css" type="text/css" />
</head>
<body>
<div class="wrapper">
    <div class="container">
        <div class="title">
            Music store
        </div>

        <header>
            <nav class="cf">
                <ul class="main-menu">
                    <li><a href="${siteRoot}" title="Home">Home</a>
                    <li><a href="${siteRoot}search" title="Search">Search</a>
                    <li><a href="${siteRoot}browse" title="Browse tracks, artists & albums">Browse</a>
                    <li><a href="${siteRoot}top" class="active" title="Weekly top">Top 10</a>
					<li><a href="${siteRoot}resources/RDFS.xml">RDFS</a>
                </ul>
                <ul class="secondary-menu">
                    <c:if test="${isAdmin}"><li><a href="${siteRoot}manager" title="Manager">Store manager</a></c:if>
                    <c:choose>
                    <c:when test="${authorized}"><li><a href="${siteRoot}logout">Logout</a></li></c:when>
                    <c:otherwise><li><a href="${siteRoot}login" title="Login">Login</a></li></c:otherwise>
                    </c:choose>
                </ul>
            </nav>
        </header>

        <div class="content top">
            <c:choose>
                <c:when test="${topList.size() > 0}">
                    <table id="results">
                        <thead>
                        <tr>
                            <c:if test="${authorized}"><th>Download</th></c:if>
                            <th># of downloads</th>
                            <th>Artist</th>
                            <th>Track</th>
                            <th>Year released</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="track" items="${topList}">
                            <tr>
                                <c:if test="${authorized}"><td><a href="${siteRoot}download?checksum=${track[6]}" class="dl">&nabla;</a></td></c:if>
                                <td>${track[1]}</td>
                                <td>${track[2]}</td>
                                <td>${track[3]}</td>
                                <td>${track[4]}</td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </c:when>
                <c:otherwise>
                    <div class="no-results">No tracks downloaded this week...</div>
                </c:otherwise>
            </c:choose>
        </div>

    </div>
</div>
<script type="text/javascript" src="${resourcesRoot}js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="${resourcesRoot}js/jquery-ui-1.9.2.custom.min.js"></script>
<script type="text/javascript" src="${resourcesRoot}js/jquery.tablesorter.min.js"></script>
<script type="text/javascript" src="${resourcesRoot}js/script.js"></script>
</body>
</html>