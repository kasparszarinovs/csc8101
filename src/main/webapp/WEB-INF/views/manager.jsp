<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<c:url var="resourcesRoot" value="/resources/" />
<c:url var="siteRoot" value="/" />
<c:set var="authorized" value="false" />
<c:set var="isAdmin" value="false" />
<sec:authorize access="isAuthenticated()"><c:set var="authorized" value="true" /></sec:authorize>
<sec:authorize access="hasRole('ROLE_ADMIN')"><c:set var="isAdmin" value="true" /></sec:authorize>

<!doctype html>
<html>
<head>
	<title>Store Manager</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	
	<link rel="stylesheet" href="${resourcesRoot}css/normalize.css" type="text/css" />
	<link rel="stylesheet" href="${resourcesRoot}css/jqui/jquery-ui-1.9.2.custom.min.css" type="text/css" />
	<link rel="stylesheet" href="${resourcesRoot}css/style.css" type="text/css" />
</head>
<body>
<div class="wrapper">
	<div class="container">
		<div class="title">Music store</div>
	
		<header>
			<nav class="cf">
				<ul class="main-menu">
					<li><a href="${siteRoot}" title="Home">Home</a>
					<li><a href="${siteRoot}search" title="Search">Search</a>
					<li><a href="${siteRoot}browse" title="Browse tracks, artists & albums">Browse</a>
					<li><a href="${siteRoot}top" title="Weekly top">Top 10</a>
					<li><a href="${siteRoot}resources/RDFS.xml">RDFS</a>
				</ul>
                <ul class="secondary-menu">
                    <c:if test="${isAdmin}"><li><a href="${siteRoot}manager" class="active" title="Manager">Store manager</a></c:if>
                    <c:choose>
                    <c:when test="${authorized}"><li><a href="${siteRoot}logout">Logout</a></li></c:when>
                    <c:otherwise><li><a href="${siteRoot}login" title="Login">Login</a></li></c:otherwise>
                    </c:choose>
                </ul>
			</nav>
		</header>
		
		<div class="content manager cf" id="manager">
			
			<aside class="manager-menu" id="manager-menu">
				<nav>
					<ul>
						<li><a href="#tab-artist" id="tab-artist-trigger" class="manager-tab-trigger active">Add artist</a></li>
						<li><a href="#tab-album" id="tab-album-trigger" class="manager-tab-trigger">Add album</a></li>
						<li><a href="#tab-track" id="tab-track-trigger" class="manager-tab-trigger">Add track</a></li>
					</ul>
				</nav>
			</aside>
		
			<div class="manager-content" id="content">
				<%-- Artist --%>
				<article class="manager-tab active" id="tab-artist">
				<form method="post">
					<h2>Add new artist</h1>
					<div class="messages"></div>
					<div class="form-fields">
						<fieldset>
							<label for="artistName">Name<span>*</span></label>
							<input type="text" name="artistName" id="artistName" required />
						</fieldset>
						<fieldset>
							<input type="hidden" name="submitArtist" value="true" />
							<input type="submit" class="submit" value="Add" />
						</fieldset>
					</div>
				</form>
				</article>
				
				<%--Album --%>
				<article class="manager-tab" id="tab-album" style="display: none;">
				<form method="post">
					<h2>Add new album</h1>
					<div class="messages"></div>
					<div class="form-fields">
						<fieldset>
							<label for="albumName">Name<span>*</span></label>
							<input type="text" name="albumName" id="albumName" required />
						</fieldset>
						<fieldset>
							<label for="albumArtist">Artist<span>*</span></label>
							<input type="text" name="albumArtist" id="albumArtist" class="artistName" required />
						</fieldset>
						<fieldset>
							<label for="albumYear">Year<span>*</span></label>
							<input type="text" name="albumYear" id="albumYear" required />
						</fieldset>
						<fieldset>
							<input type="hidden" name="submitAlbum" value="true" />
							<input type="submit" class="submit" value="Add" />
						</fieldset>
					</div>
				</form>
				</article>
				
				<%--Track --%>
				<article class="manager-tab" id="tab-track" style="display: none;">
				<form method="post">
					<h2>Add new track</h1>
					<div class="messages"></div>
					<div class="form-fields">
						<fieldset>
							<label for="trackName">Name<span>*</span></label>
							<input type="text" name="trackName" id="trackName" required />
						</fieldset>
						<fieldset>
							<label for="trackArtist">Artist<span>*</span></label>
							<input type="text" name="trackArtist" id="trackArtist" class="artistName" required />
						</fieldset>
						<fieldset>
							<label for="trackAlbum">Album</label>
							<input type="text" name="trackAlbum" id="trackAlbum" class="albumName" />
						</fieldset>
						<fieldset>
							<label for="trackYear">Year<span>*</span></label>
							<input type="text" name="trackYear" id="trackYear" required />
						</fieldset>
						<fieldset>
							<input type="hidden" name="submitTrack" value="true" />
							<input type="submit" class="submit" value="Add" />
						</fieldset>
					</div>
				</form>
				</article>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="${resourcesRoot}js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="${resourcesRoot}js/jquery-ui-1.9.2.custom.min.js"></script>
<script type="text/javascript" src="${resourcesRoot}js/jquery.tablesorter.min.js"></script>
<script type="text/javascript" src="${resourcesRoot}js/script.js"></script>
</body>
</html>